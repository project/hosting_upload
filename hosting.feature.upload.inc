<?php
// $Id$
/**
 * @file
 *   hosting_upload feature definition.
 * 
 */

/**
 * Register a hosting feature with Aegir.
 *
 * This will be used to generate the 'admin/hosting' page.
 * 
 * @return 
 *  associative array indexed by feature key.
 */
function hosting_upload_hosting_feature() {
  $features['hosting_upload'] = array(
    // title to display in form
    'title' => t('Hosting upload'),
    // description
    'description' => t('Enable uploading platforms and extensions.'),
    // initial status ( HOSTING_FEATURE_DISABLED, HOSTING_FEATURE_ENABLED, HOSTING_FEATURE_REQUIRED )
    'status' => HOSTING_FEATURE_DISABLED,
    // module to enable/disable alongside feature
    'module' => 'hosting_upload',
    // associate with a specific node type.
    //  'node' => 'nodetype',
    // which group to display in ( null , experimental )
    'group' => 'experimental'
    );
  return $features;
}
