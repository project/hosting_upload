<?php

/**
 * @file         hosting_upload.drush.inc
 * @author       Erlend ter Maat
 * @licence      GNU General Public Licence v2
 * @description  Drush hooks
 */
require_once(dirname(__FILE__) . '/include/functions.php');
require_once(dirname(__FILE__) . '/include/platform.php');

/**
 * Implement the hook_deploy_pre_hosting_task
 * 
 */
function drush_hosting_upload_pre_hosting_task() {

  // get the task
  $task = & drush_get_context('HOSTING_TASK');

  if ($task->ref->type == 'platform' && $task->task_type == 'verify') {
    $result = h12d_prepare_platform($task->ref->nid, $task->ref->publish_path);

    // workaround, the status frose on error
    if ($result <> 'OK') {
      h12d_set_task_status($task->vid, 2);
    }
  }
}

/**
 * Rollback function of hook_pre_hosting_task
 */
function drush_hosting_upload_pre_hosting_task_rollback() {
  // get the task
  $task = & drush_get_context('HOSTING_TASK');

  // cleanup posible mess
  $mess = $task->ref->publishpath;

  if (file_exists("$mess.tmp")) {
    h12d_remove_directory("$mess.tmp");
  }
  if (file_exists("$mess.tmp2")) {
    h12d_remove_directory("$mess.tmp2");
  }

  h12d_set_task_status($task->vid, 2);
}
