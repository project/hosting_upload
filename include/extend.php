<?php
/**
 * functions for extending sites
 */

function hosting_task_extend_form($node) {
  $form = array(
    // hidden field - type of extend; platform / site
    // hidden field - id of the extendible (or would the drush alias be better?)
  );
  
  return $form;
}

function hosting_task_extend_form_validate($form, &$form_state) {
  // process the parameters
}

function hosting_task_extend_form_submit($form, &$form_state) {
  // create a task using hosting_add_task
}