<?php

/**
 * @file         functions.php
 * @author       Erlend ter Maat
 * @licence      GNU General Public Licence v2
 * @description  Helper functions
 * 
 * All functions here start with the
 * abbrieviation of hosting_upload, the first character, 12 characters and
 * the final character. This makes h12d_<function_name>
 * 
 * General policy for this project: when it touches drupal use module name,
 * use the abbrieviation otherwise.
 * 
 */

/**
 * Extract an archive using shell commands
 *
 * @param string $archive
 * @param string $target
 */
function h12d_extract_archive($archive, $target) {
  // extract
  $archive_arg = escapeshellarg($archive);
  $target_arg = escapeshellarg($target);

  `/usr/bin/7za x $archive_arg -o$target_arg`;
}

/**
 * Drop a folder using (powerfull) shell commands
 *
 * @param string $target
 */
function h12d_remove_directory($target) {
  $target_arg = escapeshellarg($target);

  `rm -rf $target_arg`;
}

/**
 * Collectively change access permissions
 * files: 644
 * folders: 755
 *
 * @param string $target
 */
function h12d_change_mod($target) {
  $target_arg = escapeshellarg($target);

  // right right
  `find $target_arg -type d -exec chmod 755 {} +`;
  `find $target_arg -type f -exec chmod 644 {} +`;
}

/**
 * Convert the hint to a non-existing filename / foldername
 *
 * @param string $dir
 * @param string $filename
 * @param string $content_type
 * @return string
 */
function h12d_unique_name($dir, $filebase, $content_type='') {

  switch ($content_type) {
    case 'application/x-tar':
    case 'application/x-gzip':
      $app = '.tgz';
      break;
    case 'application/zip':
      $app = '.zip';
      break;
    case 'application/x-7z-compressed':
      $app = '.7z';
      break;
    case 'application/x-bzip2':
      $app = '.tar.bz2';
      break;
    case '':
      // not using the content_type
      $app = '';
      break;
    default:
      $app = '';
      break;
  }

  $ext = $app;

  while (true) {
    $filename = strtolower("$filebase{$ext}");
    $filename = preg_replace('/[^a-z0-9\/.]/', '-', $filename);
    $filename = preg_replace('/-+/', '-', $filename);
    $filename = preg_replace('/\.+/', '.', $filename);

    if (!file_exists("$dir/$filename")) {
      return "$dir/$filename";
    }
    $n++;
    $ext = ".$n.$app";
  }
}

/**
 * Find drupal in a directory. Return the path
 *
 * @param string $path
 * @param integer $deep
 * @return array
 */
function h12d_here_is_drupal($path, $deep=3) {
  drupal_set_message('Deployment for ' . $path, 'info');

  if ($deep < 0) {
    return array('do' => 'nothing');
  }

  $files = new DirectoryIterator($path);

  $drupal_indicator = 0;
  $dir_count = 0;
  $file_count = 0;

  // loop trough the directory
  foreach ($files as $fileInfo) {
    if ($fileInfo->isDot()) {
      continue;
    }
    $filename = $fileInfo->getFilename();
    $pathname = $fileInfo->getPathname();
    if ($fileInfo->isDir()) {
      $dir_count++;

      switch ($filename) {
        case 'includes':
        case 'modules':
        case 'profiles':
        case 'sites':
        case 'themes':
          $drupal_indicator++;
          break;
      }
    } else if ($fileInfo->isFile()) {
      $file_count++;

      switch ($filename) {
        case 'cron.php':
        case 'index.php':
        case 'install.php':
        case 'update.php':
        case '.htaccess':
          $drupal_indicator++;
          break;
      }
    }
  }

  if ($drupal_indicator === 10) {
    return array('do' => 'move', 'path' => dirname($pathname));
  }

  if ($file_count == 1 && $dir_count == 0) {
    // might be an other archive, return do
    drupal_set_message('', 'info');
    return array('do' => 'extract', 'path' => $pathname);
  }

  if ($file_count == 0 && $dir_count == 1) {
    // go deeper
    return h12d_here_is_drupal($pathname, $deep - 1);
  }


  drupal_set_message('Nothing to report...', 'info');
  return array('do' => 'nothing(2)');
}

/**
 * Change the status of a hostmaster task
 *
 * @param integer $task
 * @param integer $status
 */
function h12d_set_task_status($task, $status=1) {
  drupal_set_message("Change task $task to status $status", 'info');
  db_query('UPDATE {hosting_task} SET task_status = %d WHERE vid = %d', $status, $task);
}

function h12d_prepare_platform($nid, $publish_path) {
  $deployment_data = h12d_get_nodedata($nid);

  $source = $deployment_data['platform_upload'];
  $destiny = $publish_path;

  if ($source == '') {
    // can it be that it just is not enabled?
    return 'OK';
  }

  if (!file_exists($source)) {
    drush_set_error('HOSTING_TASK', 'Source archive does not exist: ' . $source);
    return;
  }

  if (file_exists($destiny)) {
    drush_set_error('HOSTING_TASK', 'Publish path exists ' . $destiny);
    return;
  }

  if ($destiny[0] <> '/') {
    drush_set_error('HOSTING_TASK', 'Publish path not valid: ' . $destiny);
    return;
  }

  drupal_set_message("Extract $source", 'info');

  // extract
  h12d_extract_archive($source, "$destiny.tmp");

  if (!is_dir("$destiny.tmp")) {
    drush_set_error('HOSTING_TASK', 'Could not create tempdir (1): ' . "$destiny.tmp");
    return;
  }

  $result = h12d_here_is_drupal("$destiny.tmp");

  if ($result['do'] == 'extract') {
    drupal_set_message("Extract {$result['path']}", 'info');

    // new source file, since the other is used later on
    h12d_extract_archive($result['path'], "$destiny.tmp2");

    h12d_remove_directory("$destiny.tmp");

    rename("$destiny.tmp2", "$destiny.tmp");

    // overwrite result variable.
    $result = h12d_here_is_drupal("$destiny.tmp");

    if ($result['do'] <> 'move') {
      drush_set_error('HOSTING_TASK', 'Invalid platform');
      return;
    }
  }

  if ($result['do'] == 'move' && is_dir($result['path'])) {
    // do the move
    rename($result['path'], $destiny);

    drupal_set_message("Move codebase to $destiny", 'info');

    if (is_dir("$destiny.tmp")) {
      h12d_remove_directory("$destiny.tmp");
    }

    h12d_change_mod($destiny);

    // drop the source file
    unlink($source);
    
    // remove the record from the database, it is no longer needed.
    h12d_drop_nodedata($nid);

    return 'OK';
  }
  drush_set_error('HOSTING_TASK', 'Invalid platform');
}

/**
 * Load the path to the platforms from the settings
 * 
 * @return string
 */
function h12d_get_platform_path() {
  return variable_get('hosting_upload_platform_path', '/var/aegir/platforms');
}

/**
 * Load the path to the upload directory from the settings
 * 
 * @return string
 */
function h12d_get_upload_path() {
  return variable_get('hosting_upload_upload_path', file_directory_path() . '/hosting_upload');
}

function h12d_make_upload_path($upload_path) {
  // suppress warning if location exists allready
  @mkdir($upload_path);
  @chmod($upload_path, 0777);
}

/**
 * Load deployment specific info from the database
 *
 * @param integer $nid
 * @return array
 */
function h12d_get_nodedata($nid) {
  $result = db_query('SELECT data FROM {hosting_upload_nodedata} WHERE nid = %d', $nid);

  if ($result) {
    $data = db_result($result);
    if ($data == '') {
      return array();
    }
    return unserialize($data);
  }

  return false;
}

/**
 * Store nodedata to the database
 *
 * @param integer $nid
 * @param array $data
 */
function h12d_set_nodedata($nid, $data) {
  // remove record if exists
  h12d_drop_nodedata($nid);
  $result = db_query('INSERT INTO {hosting_upload_nodedata} (nid, data) VALUES (%d, %b)', $nid, serialize($data)
  );
}

/**
 * Drop a nodedata record from the hosting_upload table
 *
 * @param integer $nid
 */
function h12d_drop_nodedata($nid) {
  $result = db_query('DELETE FROM {hosting_upload_nodedata} WHERE nid = %d', $nid);
}

