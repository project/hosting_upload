<?php

/**
 * @file         platform.php
 * @author       Erlend ter Maat
 * @licence      GNU General Public Licence v2
 * @description  Callback functions for platforms
 */

/**
 * Implementation of the hook_form_validate
 * 
 * @param array() $form
 * @param array() $form_state
 */
function platform_form_validate($form, &$state) {
  // notify user of alternative way to import platform
  if ($state['values']['publish_path'] <> '') {
    return;
  }
  
  if ($state['values']['title'] == '') {
    form_set_error('title', t("Enter a name for the platform"));
  }
  
  /**
   * Detect file-uploads
   */
  if (is_array($_FILES) && count($_FILES) > 0) {
    foreach ($_FILES as $filedata) {
      if (array_key_exists('platform_upload', $filedata['name'])
          // but if there is no file? Then continue;
          && $filedata['error']['platform_upload'] <> 4
      ) {
        if ($filedata['error']['platform_upload'] <> 0 &&
            $filedata['tmp_name']['platform_upload'] == '') {
          form_set_error('platform_upload', t('Error while uploading file.'));
          return;
        }
        // bin go
        $state['values']['platform_upload'] = $filedata;
        break;
      }
    }
  }

  $publishpath = $state['values']['publish_path'];
  $makefile_target = '';

  if ($state['values']['platform_download'] <> '') {
    // use download things
    $makefile_target = publishpath_download(
        $state['values']['title'], $state['values']['platform_download'], h12d_get_upload_path()
    );
    if ($makefile_target == '' || $makefile_target[0] <> '/') {
      // error
      form_set_error('platform_download', $makefile_target);
      return;
    }
    $publishpath =
        h12d_unique_name(
        h12d_get_platform_path(), $state['values']['title']
    );
  } else if (is_array($state['values']['platform_upload'])) {
    // use download things
    $makefile_target = publishpath_upload(
        $state['values']['title'], $state['values']['platform_upload'], h12d_get_upload_path()
    );
    if ($makefile_target == '' || $makefile_target[0] <> '/') {
      // error
      form_set_error('platform_upload', $makefile_target);
      return;
    }
    $publishpath =
        h12d_unique_name(
        h12d_get_platform_path(), $state['values']['title']
    );
  } else if ($state['values']['publish_path'] == '') {
    form_set_error('publish_path', t('Please enter a value for <b>Publish Path</b>'));
  }

  if ($publishpath == '' || $publishpath[0] <> '/') {
    // error
    form_set_error('publish_path', t("Can't store Publish path: @value", array('@value' => $publishpath)));
  } else {
    $state['values']['publish_path'] = $publishpath;
    $state['values']['platform_upload'] = $makefile_target;
  }
}

/**
 * Update platform node from hook_nodeapi
 *
 * @param <type> $node
 */
function platform_node_update(&$node) {
  // only update if filled in.
  if ($node->platform_upload <> '') {
    h12d_set_nodedata($node->nid, array('platform_upload' => $node->platform_upload));
  }
}

/**
 * Handle downloading
 *
 * @param string $platform
 * @param string $location
 * @param string $destination
 * @return string 
 */
function publishpath_download($platform, $location, $destination) {
  // defaults
  $default['scheme'] = 'http';
  $default['host'] = 'localhost';
  $default['port'] = '80';
  $default['path'] = '/';

  // input
  $url = parse_url($location);

  $u = array_merge($default, $url);
  $rl = $u['scheme'] . '://' . $u['host'] . ':' . $u['port'];

  $dl = new WebBrowser($rl);

  $local = tempnam($destination, 'platform_');

  $result = $dl->download($u['path'], $local);

  if ($result <> $local) {
    return t('Error: could not download platform');
  }

  $type = $dl->getHeader('content_type', 'text/plain');

  $final = h12d_unique_name($destination, $platform, $type);

  rename($result, $final);
  chmod($final, 0666);

  return realpath($final);
}

/**
 * Handle uploading
 *
 * @param string $platform
 * @param string $location
 * @param string $destination
 * @return string
 */
function publishpath_upload($platform, $location, $destination) {
  $type = $location['type']['platform_upload'];

  $final = h12d_unique_name(realpath($destination), $platform, $type);
  $original = $location['tmp_name']['platform_upload'];

  if ($original <> '' && $final <> '') {
    rename($original, $final);

    if (!file_exists($final)) {
      return t('Could not rename file ' . $location['tmp_name']['platform_upload']);
    }

    chmod($final, 0666);
    return realpath($final);
  }

  return '';
}
