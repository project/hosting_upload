<?php

/**
 * @file         settings.php
 * @author       Erlend ter Maat
 * @licence      GNU General Public Licence v2
 * @description  Callback functions for module settings
 */

/**
 * hook_form_validate
 *
 * @param array() $form
 * @param array() $form_state
 */
function hosting_upload_settings_form_validate(&$form, &$form_state) {

  // check platform path
  $platform_path = $form_state['values']['platform_path'];
  if (!file_exists($platform_path)) {
    form_set_error('platform_path', t('The location \'@path\' is not writable', array('@path' => $platform_path)));
  }
}

/**
 * hook_form_submit
 *
 * @param array() $form
 * @param array() $form_state
 */
function hosting_upload_settings_form_submit(&$form, &$form_state) {
  // upload file path
  $upload_path = $form_state['values']['upload_path'];
  
  h12d_make_upload_path($upload_path);

  // check path
  if (!is_writable($upload_path)) {
    form_set_error('upload_path', t('The location \'@path\' is not writable', array('@path' => $upload_path)));
  }

  variable_set('hosting_upload_platform_path', $form_state['values']['platform_path']);
  variable_set('hosting_upload_upload_path', $form_state['values']['upload_path']);
}
